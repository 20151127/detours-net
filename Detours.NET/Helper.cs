﻿using System;

namespace DetoursNET
{
    public static class Helper
    {
        public static IntPtr GetProcAddress(string moduleName, string funcName)
        {
            return Native.WinApi.GetProcAddress(Native.WinApi.GetModuleHandle(moduleName), funcName);
        }
    }
}