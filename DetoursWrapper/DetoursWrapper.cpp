#include <Windows.h>
#include "detours.h"

extern "C"
{
	__declspec(dllexport) long DetourTransactionBeginWrapper()
	{
		return ::DetourTransactionBegin();
	}

	__declspec(dllexport) long DetourTransactionAbortWrapper()
	{
		return ::DetourTransactionAbort();
	}

	__declspec(dllexport) long DetourTransactionCommitWrapper()
	{
		return ::DetourTransactionCommit();
	}

	__declspec(dllexport) long DetourUpdateThreadWrapper(void* hThread)
	{
		return ::DetourUpdateThread(hThread);
	}

	__declspec(dllexport) long DetourAttachWrapper(void** ppPointer, void* pDetour)
	{
		return ::DetourAttach(ppPointer, pDetour);
	}

	__declspec(dllexport) long DetourDetachWrapper(void** ppPointer, void* pDetour)
	{
		return ::DetourDetach(ppPointer, pDetour);
	}
}