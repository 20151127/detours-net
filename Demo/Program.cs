﻿using System;
using System.Runtime.InteropServices;
using DetoursNET;
using Vanara.PInvoke;

namespace Demo
{
    internal class Program
    {
        static void Main()
        {
            Console.WriteLine(IntPtr.Size);

            var hook = new DetoursHook<Delegates.MessageBoxWDelegate>(
                "user32",
                "MessageBoxW",
                it =>
                {
                    return (hWnd, text, caption, type) =>
                    {
                        return it.OrigFuncDelegate(hWnd, "Hooked ====> " + text, "Hooked ====> " + caption, type);
                    };
                });

            using (hook)
            {
                var res = User32.MessageBox(IntPtr.Zero, "TEST MESSAGE 中文", "Title", User32.MB_FLAGS.MB_OKCANCEL | User32.MB_FLAGS.MB_ICONQUESTION);

                if (res == User32.MB_RESULT.IDOK)
                {
                    MessageBoxW(IntPtr.Zero, "TEST MESSAGE 中文", "Title", (uint)(User32.MB_FLAGS.MB_OK | User32.MB_FLAGS.MB_ICONINFORMATION));
                }
            }
        }


        [DllImport("user32", SetLastError = true, CharSet = CharSet.Unicode)]
        static extern int MessageBoxW(IntPtr hWnd, string text, string caption, uint type);
    }

    static class Delegates
    {
        [UnmanagedFunctionPointer(CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public delegate int MessageBoxWDelegate(IntPtr hWnd, string text, string caption, uint type);
    }
}